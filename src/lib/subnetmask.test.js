import { describe, it, expect } from 'vitest';
import { subnetmask } from './subnetmask';

describe('hostbits test', () => {
    it('24 -> 8', () => {
        expect(subnetmask.hostbits(24)).toBe(8);
    });
});
